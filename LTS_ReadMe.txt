LiveTrackingServer Project



1. Repository

Location: https://bitbucket.org/Corandus/livetrackingserver
It is divided into 4 modules: Interface, Persistence, Process and Webapp
Each module is a repository

2. Cloning a repository

Download sourcetree or github and enter the terminal
In the terminal, navigate where you want to clone a module and run "git clone <repository url>"

3. Importing repository to eclipse

Since we don't commit and push ".settings/" and ".project" files, you can not simply go to eclipse and click "Import project"
Instead, you will create a new project (dynamic web project for LTS_Webapp and java project for the other 3) and select the previously cloned directory, naming your project the same as the module.

4. Plugins

To run maven, you have to install the "Maven Integration for Eclipse" plugin from the Eclipse Marketplace. After that, you can build a project by righ-clicking the project's pom.xml file and selecting "Run as... -> Maven install". This will generate a .jar / .war file in the "target/" directory

5. Dependencies between modules

LTS_Interface and LTS_Persistence cand be built without the need of including any .jars from the other modules
LTS_Process must include the generated .jars from LTS_Interface and LTS_Persistence. The .classpath file specifies where the .jars are located. At the moment, LTS_Process looks for them one level back and into the LTS_Interface/target and LTS_Persistence respectively.
For this to work properly, you must ensure that your projects are in the same directory.
LTS_Webapp will need all 3 .jars from the other modules and, as mentioned above, it will automatically find them if all projects are in the same directory

6. Deploying application localy
First you need to install an application server, for instance: wildfly. After that, in eclipse, right-click on the LTS_Webapp project and select "Run as... -> Run on server". At first, you will be asked to specify which server you would like to run the application on, so select wildfly server.

7. Deploying application on the Corandus server

Soon this will be done automatically by Jenkins but for now, you will have to go to Jenkins (http://79.143.182.244:7777/), click the LiveTrackingServer job and click "Build now". This will take care of the dependencies between the 4 modules and deploy the final generated .war file.
But, in order for this to work, you must ensure that wildfly is running on the Corandus server. Log in as root, navigate to the wildfly installation directory / bin (/opt/wildfly/bin) and run standalone.sh
Application will be available at http://79.143.182.244:8180/